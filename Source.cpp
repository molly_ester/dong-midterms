#include<iostream>
#include<string>
#include<conio.h>
#include<time.h>
#include "OrderedArray.h"
#include "UnorderedArray.h"

using namespace std;

int main() {
	srand(time(NULL));
	int size;
	cout << "Enter Size for the Dynamic Array: ";
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++) {
		int random = rand() % 100 + 1;
		unordered.push(random);
		ordered.push(random);
	}

	system("CLS");

	while (1) {
		int input;
		cout << "Ordered Array: ";
		for (int i = 0; i < ordered.getSize(); i++) {
			if (i % 11 == 10) cout << endl << "\t\t";
			cout << ordered[i] << " ";
		}
		cout << endl << "Unordered Array: "; 
		for (int i = 0; i < ordered.getSize(); i++) {
			if (i % 11 == 10) cout << endl << "\t\t";
			cout << unordered[i] << " ";
		}
		cout << endl << endl;

		cout << "What do you want to do ?\n1 - Remove element at index\n2 - Search for element\n3 - Expand and generate random values" << endl;
		do cin >> input; while (input > 3 || input < 1);
		if (input == 1) {
			cout << "\nWhich element would you like to remove: ";
			do cin >> input; while (input >= ordered.getSize() || input >= unordered.getSize() || input < 0);
			unordered.remove(input);
			ordered.remove(input);

			cout << "successfully removed index " << input << endl;
			_getch();
			system("CLS");
		}
		else if (input == 2) {
			cout << "Input Value to Search: ";
			cin >> input;

			cout << "Ordered Array: ";
			if (ordered.binarySearch(input) == -1) cout << "Value not found.\t";
			else cout << "Value found at element " << ordered.binarySearch(input) << endl;
			cout << "Unordered Array: ";
			if (unordered.linearSearch(input) == -1) cout << "Value not found.\t";
			else cout << "Value found at element " << unordered.linearSearch(input);
			cout << endl;
			_getch();
			system("CLS");
		}
		else {
			cout << "Input Size of Expansion: ";
			cin >> size;
			
			for (int i = 0; i < size; i++) {
				int random = rand() % 100 + 1;
				unordered.push(random);
				ordered.push(random);
			}

			cout << "\nThe Array Has been Expanded\n";
			cout << "Ordered Array: ";
			for (int i = 0; i < ordered.getSize(); i++) {
				if (i % 11 == 10) cout << endl << "\t\t";
				cout << ordered[i] << " ";
			}
			cout << endl << "Unordered Array: ";
			for (int i = 0; i < ordered.getSize(); i++) {
				if (i % 11 == 10) cout << endl << "\t\t";
				cout << unordered[i] << " ";
			}
			_getch();
			system("CLS");
		}
	}
}